package com.exadel.placebook.model.enums;

public enum Role {
    ADMIN,
    USER
}
